//TODO: Add Session Tokens to filter questions.
//TODO: Add error handling based on response code.

//Checks retrives a trivia based on arguments given in configure page.
export function getTrivia(amount, category, difficulty) {
    let url = `https://opentdb.com/api.php?amount=${amount}`;

    category ? (url += `&category=${category}`) : url;
    difficulty ? (url += `&difficulty=${difficulty}`) : url;
    return (
        fetch(url)
            .then((response) => response.json())
            .then((response) => response.results)
            // Takes all the awnsers and put them into a single array
            .then((response) => {
                return response.map((question) => {
                    question.answers = [
                        question.correct_answer,
                        ...question.incorrect_answers,
                    ];
                    question.answers = shuffleArray(question.answers);
                    return question;
                });
            })
    );
}

//Helper function to shuffle the awnsers so the correct one isn't always on top.
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}
