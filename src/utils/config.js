import { STORAGE_KEY_CONFIG } from '../consts/storageKeys';
import { RESULTS_KEY_CONFIG } from '../consts/storageKeys';

//Gets the trivia configuration from local storage of browser
export function getConfig() {
    const configKey = localStorage.getItem(STORAGE_KEY_CONFIG);
    return JSON.parse(configKey);
}

//Set the trivia configuration in local storage of browser
export function setConfig(config) {
    localStorage.setItem(STORAGE_KEY_CONFIG, JSON.stringify(config));
}

//Gets the trivia results from local storage of browser
export function getResult() {
    const resultKey = localStorage.getItem(RESULTS_KEY_CONFIG);
    return JSON.parse(resultKey);
}

//Set the trivia results in local storage of browser
export function setResult(result) {
    localStorage.setItem(RESULTS_KEY_CONFIG, JSON.stringify(result));
}
