//Gets all the categories from the api helper.
export function getCategories() {
    return fetch('https://opentdb.com/api_category.php')
        .then((response) => {
            if (response.status !== 200) {
                throw new Error('Could not fetch categories');
            }
            return response.json();
        })
        .then((response) => response.trivia_categories);
}
