import { createRouter, createWebHashHistory } from 'vue-router';
import TriviaConfigure from './components/TriviaConfigure.vue';
import TriviaList from './components/TriviaList.vue';
import TriviaResult from './components/TriviaResult.vue';

const routes = [
    {
        path: '/',
        name: 'Configure',
        component: TriviaConfigure,
    },
    {
        path: '/trivia',
        name: 'Trivia',
        component: TriviaList,
    },
    {
        path: '/result',
        name: 'Result',
        component: TriviaResult,
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

export default router;
